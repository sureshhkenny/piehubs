import { Request, Response } from "express";
import bcrypt from "bcrypt";
import nodemailer from "nodemailer";
import jwt from "jsonwebtoken";
import smtp from "../config/mailer";
import { User } from "../model/Auth/M_User";
import { Student } from "../model/Auth/M_Student";
import {
  updateOtp,
  updateVerify,
  getUser,
  resetPassword,
} from "../services/UserService";

export default {
  signup: async (req: Request, res: Response) => {
    try {
      const { email, mobile } = req.body;
      const users = await User.findOneBy({ email });
      if (users?.email_verified) {
        res.json({ success: false, msg: "Email already exists" });
      } else {
        let transporter = nodemailer.createTransport({ ...smtp });
        let otp = Math.floor(100000 + Math.random() * 900000);
        transporter
          .sendMail({
            from: "genie@truetechsolutions.in",
            to: email,
            subject: "Email OTP Verification",
            html: `<!DOCTYPE html><html><head><title></title></head><body><div><p>Hello,</p><p>Welcome to Pie Hub Labs</p><p>Please use the following OTP ${otp} to complete your sign-up process.</p><p>Enter the code and get connected.</p><p>Best Wishes,</p><p>Pie Hub Labs</p></div></body></html>`,
          })
          .catch(console.error);
        if (!users) {
          const user = User.create({
            email,
            mobile,
            otp,
          });
          await user.save();
        } else if (users.id) {
          updateOtp(otp, users.id);
        }
        res
          .status(201)
          .json({ success: true, msg: "Please verify email", otp });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },

  signin: async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      const user = await User.findOneBy({ email });
      if (!user) {
        res.json({ success: false, msg: "Invalid credentials" }); // Can't find user
      } else {
        if (
          (await bcrypt.compare(password, `${user.password}`)) &&
          (user.email_verified || user.user_role == "admin")
        ) {
          const token = jwt.sign(
            { userId: user.id },
            `${process.env.ACCESS_TOKEN_SECRET}`
          );
          if (user.user_role == "student") {
            const student = await Student.findOneBy({ user_id: user.id });
            res.status(200).json({
              success: true,
              msg: "Successfully logged in",
              token,
              student,
            });
          } else {
            res.status(200).json({
              success: true,
              msg: "Successfully logged in",
              token,
              user_id: user.id,
            });
          }
        } else {
          res.json({ success: false, msg: "Invalid credentials" }); // Incorrect Password or OTP not verified
        }
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },

  verification: async (req: Request, res: Response) => {
    try {
      const { email, otp } = req.body;
      if (email && otp) {
        const user = await User.findOneBy({ email });
        if (!user || user.otp != otp) {
          res.json({ success: false, msg: "Invalid credentials" }); // invalid user or otp not match
        } else {
          res.status(200).json({
            success: true,
            msg: "Otp Verified Successfully",
            id: user.id,
          });
        }
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },

  register: async (req: Request, res: Response) => {
    try {
      const { first_name, last_name, password, school_name, standard } =
        req.body;
      const { user_id } = req.params;
      const hashpassword = await bcrypt.hash(password, 10);
      const user = await getUser(user_id);
      if (!user) {
        res.json({ success: false, msg: "Invalid User" });
      } else {
        const student = Student.create({
          first_name,
          last_name,
          school_name,
          standard,
          mobile: user.mobile,
          user_id,
        });
        await student.save();
        updateVerify(user_id, hashpassword);
        const token = jwt.sign(
          { userId: user.id },
          `${process.env.ACCESS_TOKEN_SECRET}`
        );
        res.status(200).json({
          success: true,
          msg: "Successfully registered",
          token,
          student,
        });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  get: async (req: Request, res: Response) => {
    const user = await User.find();
    res.json({ success: true, msg: "success", user });
  },
  admin: async (req: Request, res: Response) => {
    const { email, password } = req.body;
    try {
      const hashpassword = await bcrypt.hash(password, 10);
      const user = User.create({
        email,
        password: hashpassword,
        user_role: "admin",
        mobile: 9876543210,
      });
      await user.save();
      const token = jwt.sign(
        { userId: user.id },
        `${process.env.ACCESS_TOKEN_SECRET}`
      );
      res.status(201).json({ success: true, token });
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  forgot_password: async (req: Request, res: Response) => {
    try {
      const { email } = req.body;
      const user = await User.findOneBy({ email });
      if (user?.id) {
        let otp = Math.floor(100000 + Math.random() * 900000);
        updateOtp(otp, user.id);
        res.json({ success: true, otp, user });
      } else {
        res.json({ success: false, msg: "invalid credentials" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  reset_password: async (req: Request, res: Response) => {
    try {
      const { user_id, otp, password } = req.body;
      const user = await getUser(user_id);
      if (!user || user.otp != otp) {
        res.json({ success: false, msg: "invalid credentials" });
      } else {
        const hashpassword = await bcrypt.hash(password, 10);
        resetPassword(user_id, hashpassword);
        res.json({ success: true, msg: "Password reset Successfully", user });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
};
