import { createQueryBuilder } from "typeorm";
import { eachWeekOfInterval } from "date-fns";
import { M_Goal } from "../model/Master/M_Goal";
import { User } from "../model/Auth/M_User";
import { Activity } from "../model/Master/M_Activity";
import { Team } from "../model/Master/M_Team";
import { A_Social_Like } from "../model/Activity/A_Social_Like";
import { Subject } from "../model/Master/M_Subject";
import { Chapter } from "../model/Master/M_Chapter";
import { Topic } from "../model/Master/M_Topic";
import { SubTopic } from "../model/Master/M_SubTopic";
import { TeamMember } from "../model/Activity/T_Team_Members";
import { Student } from "../model/Auth/M_Student";
import { A_Social } from "../model/Activity/A_Social";
import { U_Activity } from "../model/User/U_Activity";

function getUser(id: string) {
  return User.findOneById(id);
}

function getGoal(id: string) {
  return M_Goal.findOneById(id);
}

function getActivity(id: string) {
  return Activity.findOneById(id);
}

function getSocialLike(uId: string, sId: string) {
  return createQueryBuilder(A_Social_Like, "a_social_like")
    .where("user_id=:uId", { uId })
    .andWhere("social_activity_id=:sId", {
      sId,
    })
    .execute();
}

function deleteLike(uId: string, sId: string) {
  createQueryBuilder(A_Social_Like, "a_social_like")
    .delete()
    .where("user_id=:uId", { uId })
    .andWhere("social_activity_id=:sId", {
      sId,
    })
    .execute();
}

function updateOtp(otp: number, id: string) {
  createQueryBuilder(User, "m_user")
    .update(User)
    .set({ otp })
    .where("id = :id", { id })
    .execute();
}

function resetPassword(id: String, password: string) {
  createQueryBuilder(User, "m_user")
    .update(User)
    .set({ password })
    .where("id = :id", { id })
    .execute();
}

function updateVerify(id: string, password: string) {
  createQueryBuilder(User, "m_user")
    .update(User)
    .set({ email_verified: true, password })
    .where("id = :id", { id })
    .execute();
}

function getSubjects(id: string) {
  return createQueryBuilder(Subject, "m_subject")
    .where("m_subject.class_id = :id", { id })
    .getMany();
}

function getChapter() {
  return Chapter.find({
    order: {
      category_name: "DESC",
    },
  });
}

function getChapters(id: string) {
  return createQueryBuilder(Chapter, "m_chapter")
    .where("m_chapter.subject_id = :id", { id })
    .getMany();
}

function getTopic(id: string) {
  return createQueryBuilder(Topic, "m_topic")
    .where("m_topic.chapter_id = :id", { id })
    .getMany();
}

function getSubtopic(id: string) {
  return createQueryBuilder(SubTopic, "m_subtopic")
    .where("m_subtopic.topic_id=:id", { id })
    .getMany();
}

function getMember(id: string) {
  return createQueryBuilder(TeamMember, "team_members")
    .where("team_members.team_id=:id", { id })
    .getMany();
}

function getTeamAtom() {
  return Team.find({ order: { team_atoms: "DESC" } });
}

function getActivityAtom() {
  return Activity.find({ order: { atoms: "DESC" } });
}

function getStudent() {
  return Student.find({ order: { atoms: "DESC" } });
}

function getStudentById(id: string) {
  return Student.findOneById(id);
}

function deleteMember(id: string, tId: string) {
  createQueryBuilder(TeamMember, "team_members")
    .delete()
    .where("team_members.team_id=:tId", { tId })
    .andWhere("team_members.user_id=:id", { id })
    .execute();
}

function deleteSocial(aId: string, sId: string) {
  createQueryBuilder(A_Social, "a_social")
    .delete()
    .where("a_social.activity_id=:aId", { aId })
    .andWhere("a_social.social_activity_id=:sId", { sId })
    .execute();
}

function getAct(id: string) {
  return createQueryBuilder(Activity, "m_activity")
    .where("m_activity.chapter_id=:id", { id })
    .getMany();
}
function getActUser(id: string) {
  return createQueryBuilder(Activity, "m_activity")
    .where("m_activity.created_by=:id", { id })
    .getMany();
}

function getWeek() {
  const week = eachWeekOfInterval(
    {
      start: new Date(2022, 8, 1),
      end: new Date(2023, 10, 5),
    },
    { weekStartsOn: 2 }
  );
  return week;
}

const getMembers = async (id: string) => {
  return await createQueryBuilder(TeamMember, "team_members")
    .where("team_members.user_id=:id", { id })
    .getMany();
};

function uploadVideo(id: string, aId: string, url: string) {
  const video_url = `https://piehub.s3.ap-south-1.amazonaws.com/${url}`;
  createQueryBuilder(U_Activity, "u_activity")
    .update(U_Activity)
    .set({ video_url })
    .where("user_id = :id", { id })
    .andWhere("activity_id=:aId", { aId })
    .execute();
}

export {
  getUser,
  getGoal,
  getActivity,
  getSocialLike,
  deleteLike,
  updateOtp,
  updateVerify,
  getSubjects,
  getChapter,
  getChapters,
  getTopic,
  getSubtopic,
  getMember,
  getTeamAtom,
  getActivityAtom,
  getStudent,
  deleteMember,
  deleteSocial,
  getAct,
  getActUser,
  getWeek,
  getMembers,
  resetPassword,
  getStudentById,
  uploadVideo,
};
