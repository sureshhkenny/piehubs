import { Column, Entity } from "typeorm";
import { Base } from "../Base";

@Entity("t_goal_activity")
export class GoalActivity extends Base {
  @Column()
  goal_id?: string;
  @Column()
  activity_id?: string;
}
