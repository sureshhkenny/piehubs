"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getObjectSignedUrl = exports.deleteFile = exports.uploadFile = void 0;
const client_s3_1 = require("@aws-sdk/client-s3");
const s3_request_presigner_1 = require("@aws-sdk/s3-request-presigner");
const dotenv_1 = require("dotenv");
(0, dotenv_1.config)();
const s3 = new client_s3_1.S3Client({
    region: `${process.env.region}`,
    credentials: {
        accessKeyId: `${process.env.accessKeyId}`,
        secretAccessKey: `${process.env.secretAccessKey}`,
    },
});
function uploadFile(file, name) {
    const uploadParams = {
        Bucket: `${process.env.bucket_name}`,
        Body: file.buffer,
        Key: name,
        ContentType: file.mimetype,
    };
    return s3.send(new client_s3_1.PutObjectCommand(uploadParams));
}
exports.uploadFile = uploadFile;
function deleteFile(fileName) {
    const deleteParams = {
        Bucket: `${process.env.bucket_name}`,
        Key: fileName,
    };
    return s3.send(new client_s3_1.DeleteObjectCommand(deleteParams));
}
exports.deleteFile = deleteFile;
function getObjectSignedUrl(key) {
    return __awaiter(this, void 0, void 0, function* () {
        const params = {
            Bucket: `${process.env.bucket_name}`,
            Key: key,
        };
        const command = new client_s3_1.GetObjectCommand(params);
        const url = yield (0, s3_request_presigner_1.getSignedUrl)(s3, command);
        return url;
    });
}
exports.getObjectSignedUrl = getObjectSignedUrl;
//# sourceMappingURL=s3_bucket.js.map