"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const M_User_1 = require("../model/Auth/M_User");
exports.default = (req, res, next) => {
    const token = req.header("authorization");
    if (token) {
        const check = token.replace("Bearer ", "");
        jsonwebtoken_1.default.verify(check, `${process.env.ACCESS_TOKEN_SECRET}`, (err, payload) => __awaiter(void 0, void 0, void 0, function* () {
            if (err) {
                res.json({ success: false, msg: "You must be login" });
            }
            else {
                const { userId } = payload;
                const user = yield M_User_1.User.findOneById(userId);
                if ((user === null || user === void 0 ? void 0 : user.user_role) == "student") {
                    req.user = user;
                    next();
                }
                else if ((user === null || user === void 0 ? void 0 : user.user_role) == "admin") {
                    res.json({ success: false, msg: "You must be login with student" });
                }
                else {
                    res.json({ success: false, msg: "You must be login" });
                }
            }
        }));
    }
    else {
        res.json({ success: false, msg: "You must be login" });
    }
};
//# sourceMappingURL=auth_student.js.map