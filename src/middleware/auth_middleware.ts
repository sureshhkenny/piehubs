import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { User } from "../model/Auth/M_User";

export interface IGetUserAuthInfoRequest extends Request {
  user?: any; // or any other type
}

export default (
  req: IGetUserAuthInfoRequest,
  res: Response,
  next: NextFunction
) => {
  const token = req.header("authorization");
  if (token) {
    const check = token.replace("Bearer ", "");
    jwt.verify(
      check,
      `${process.env.ACCESS_TOKEN_SECRET}`,
      async (err, payload) => {
        if (err) {
          res.json({ success: false, msg: "You must be login" }); //token is invalid
        } else {
          const { userId }: any = payload;
          const user = await User.findOneById(userId);
          if (user?.user_role == "student") {
            res.json({ success: false, msg: "You must be login with admin" });
          } else if (user?.user_role == "admin") {
            req.user = user;
            next();
          } else {
            res.json({ success: false, msg: "You must be login" });
          }
        }
      }
    );
  } else {
    res.json({ success: false, msg: "You must be login" }); //token is required
  }
};
