import {
  S3Client,
  PutObjectCommand,
  DeleteObjectCommand,
  GetObjectCommand,
} from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";
import { config } from "dotenv";
config();
const s3 = new S3Client({
  region: `${process.env.region}`,
  credentials: {
    accessKeyId: `${process.env.accessKeyId}`,
    secretAccessKey: `${process.env.secretAccessKey}`,
  },
});

export function uploadFile(file, name) {
  const uploadParams = {
    Bucket: `${process.env.bucket_name}`,
    Body: file.buffer,
    Key: name,
    ContentType: file.mimetype,
  };
  return s3.send(new PutObjectCommand(uploadParams));
}

export function deleteFile(fileName) {
  const deleteParams = {
    Bucket: `${process.env.bucket_name}`,
    Key: fileName,
  };
  return s3.send(new DeleteObjectCommand(deleteParams));
}

export async function getObjectSignedUrl(key) {
  const params = {
    Bucket: `${process.env.bucket_name}`,
    Key: key,
  };

  // https://aws.amazon.com/blogs/developer/generate-presigned-url-modular-aws-sdk-javascript/
  const command = new GetObjectCommand(params);
  const url = await getSignedUrl(s3, command);
  // const seconds = 60
  // const url = await getSignedUrl(s3, command, { expiresIn: seconds });
  return url;
}

// import multer from "multer";
// import multerS3 from "multer-s3";
// import path from "path";

// export const upload = multer({
//   storage: multerS3({
//     s3,
//     bucket: `${process.env.bucket_name}`,
//     acl: "public-read",
//     metadata: function (req, file, cb) {
//       console.log("@@@");
//       cb(null, { fieldName: file.fieldname });
//     },
//     key: function (req, file, cb) {
//       console.log("===");
//       cb(null, Date.now().toString() + path.extname(file.originalname));
//     },
//   }),
// });
