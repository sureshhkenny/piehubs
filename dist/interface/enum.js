"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionType = void 0;
var TransactionType;
(function (TransactionType) {
    TransactionType["LIKE"] = "like";
    TransactionType["UNLIKE"] = "unlike";
})(TransactionType = exports.TransactionType || (exports.TransactionType = {}));
//# sourceMappingURL=enum.js.map