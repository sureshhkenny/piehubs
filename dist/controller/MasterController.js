"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const M_Class_1 = require("../model/Master/M_Class");
const M_Subject_1 = require("../model/Master/M_Subject");
const M_Chapter_1 = require("../model/Master/M_Chapter");
const M_Topic_1 = require("../model/Master/M_Topic");
const M_SubTopic_1 = require("../model/Master/M_SubTopic");
const M_Activity_1 = require("../model/Master/M_Activity");
const M_Goal_1 = require("../model/Master/M_Goal");
const GoalActivity_1 = require("../model/Transaction/GoalActivity");
const UserService_1 = require("../services/UserService");
const typeorm_1 = require("typeorm");
const U_Activity_1 = require("../model/User/U_Activity");
exports.default = {
    class_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const stnds = yield M_Class_1.Class.find();
            res.json({ success: true, class: stnds });
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    class_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const stnds = M_Class_1.Class.create({
                class_name: req.body.class_name,
            });
            yield stnds.save();
            res.status(201).json({ success: true, msg: "Class added successfully" });
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    subject_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { class_id } = req.params;
            const classId = yield M_Class_1.Class.findOneById(class_id);
            const subject = yield (0, UserService_1.getSubjects)(class_id);
            if (!classId || !subject) {
                res.json({
                    success: false,
                    msg: "Class not found or no subject for this class",
                });
            }
            else {
                res.json({ success: true, subject });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    subject_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { class_id } = req.params;
            const classes = yield M_Class_1.Class.findOneById(class_id);
            if (!classes) {
                res.json({ success: false, msg: "Class not found" });
            }
            else {
                const subject = M_Subject_1.Subject.create({
                    subject_name: req.body.subject_name,
                    classes,
                });
                yield subject.save();
                res
                    .status(201)
                    .json({ success: true, msg: "Subject added Successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    subject_read: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const subject = yield M_Subject_1.Subject.find();
            if (!subject) {
                res.json({ success: false, msg: "Subject not found" });
            }
            else {
                res.json({ success: true, subject });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    chapter_read: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const chapter = yield M_Chapter_1.Chapter.find();
            if (!chapter) {
                res.json({ success: false, msg: "Chapter not found" });
            }
            else {
                res.json({ success: true, chapter });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    chapter_category: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const chapter = (0, UserService_1.getChapter)();
            if (!chapter) {
                res.json({ success: false, msg: "no chapter found" });
            }
            else {
                res.send({ success: true, chapter });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    chapter_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { subject_id } = req.params;
            const subId = yield M_Subject_1.Subject.findOneById(subject_id);
            const chapter = yield (0, UserService_1.getChapters)(subject_id);
            if (!subId || !chapter) {
                res.json({ success: false, msg: "Subject or chapter not found" });
            }
            else {
                res.json({ success: true, chapter });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    chapter_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { subject_id } = req.params;
            const { chapter_name, category_name } = req.body;
            const subject = yield M_Subject_1.Subject.findOneById(subject_id);
            if (!subject) {
                res.json({ success: false, msg: "Subject not found" });
            }
            else {
                const chapter = M_Chapter_1.Chapter.create({
                    chapter_name,
                    category_name,
                    subject,
                });
                yield chapter.save();
                res
                    .status(201)
                    .json({ success: true, msg: "Chapter added Successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    chapter_one_category: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { name } = req.params;
            const chapter = yield M_Chapter_1.Chapter.find({ where: { category_name: name } });
            if (chapter.length < 1) {
                res.json({ success: false, msg: "chapter not found" });
            }
            else {
                res.json({ success: true, chapter });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    topic_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { chapter_id } = req.params;
            const chapterId = yield M_Chapter_1.Chapter.findOneById(chapter_id);
            const topic = yield (0, UserService_1.getTopic)(chapter_id);
            if (!chapterId || !topic) {
                res.json({ success: false, msg: "Chapter or topic not found" });
            }
            else {
                res.json({ success: true, topic });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    topic_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { chapter_id } = req.params;
            const chapter = yield M_Chapter_1.Chapter.findOneById(chapter_id);
            if (!chapter) {
                res.json({ success: false, msg: "Chapter not found" });
            }
            else {
                const topic = M_Topic_1.Topic.create({
                    topic_name: req.body.topic_name,
                    chapter,
                });
                yield topic.save();
                res
                    .status(201)
                    .json({ success: true, msg: "Topic added Successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    topic_read: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const topic = yield M_Topic_1.Topic.find();
            if (!topic) {
                res.json({ success: false, msg: "Topic not found" });
            }
            else {
                res.json({ success: true, topic });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    subtopic_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { topic_id } = req.params;
            const topicId = yield M_Topic_1.Topic.findOneById(topic_id);
            const subtopic = yield (0, UserService_1.getSubtopic)(topic_id);
            if (!topicId || !subtopic) {
                res.json({ success: false, msg: "Topic not found" });
            }
            else {
                res.json({ success: true, subtopic });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    subtopic_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { topic_id } = req.params;
            const { subtopic_name, url } = req.body;
            const topic = yield M_Topic_1.Topic.findOneById(topic_id);
            if (!topic) {
                res.json({ success: false, msg: "Topic not found" });
            }
            else {
                const subtopic = M_SubTopic_1.SubTopic.create({
                    subtopic_name,
                    url,
                    topic,
                });
                yield subtopic.save();
                res
                    .status(201)
                    .json({ success: true, msg: "Subtopic added Successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    subtopic_read: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const subtopic = yield M_SubTopic_1.SubTopic.find();
            if (!subtopic) {
                res.json({ success: false, msg: "Subtopics not found" });
            }
            else {
                res.json({ success: true, subtopic });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_read: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const activity = yield M_Activity_1.Activity.find();
            if (!activity) {
                res.json({ success: false, msg: "Activity not found" });
            }
            else {
                res.json({ success: true, activity });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { chapter_id } = req.params;
            const chapter = yield M_Chapter_1.Chapter.findOneById(chapter_id);
            if (!chapter) {
                res.json({ success: false, msg: "Activity not found" });
            }
            else {
                const activity = yield (0, UserService_1.getAct)(chapter_id);
                res.json({ success: true, activity });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_getUser: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { class_id, subject_id, chapter_id } = req.body;
            const { user_id } = req.params;
            const stnd = yield M_Class_1.Class.findOneById(class_id);
            const subject = yield M_Subject_1.Subject.findOneById(subject_id);
            const chapter = yield M_Chapter_1.Chapter.findOneById(chapter_id);
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!stnd || !subject || !chapter || !user) {
                res.json({ success: false, msg: "Activity not found" });
            }
            else {
                const activity = yield (0, UserService_1.getActUser)(user_id);
                res.json({ success: true, activity });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { class_id, subject_id, chapter_id, activity_name, atoms, url } = req.body;
            const { user_id } = req.params;
            const classes = yield M_Class_1.Class.findOneById(class_id);
            const subject = yield M_Subject_1.Subject.findOneById(subject_id);
            const chapter = yield M_Chapter_1.Chapter.findOneById(chapter_id);
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!classes || !subject || !chapter || !user) {
                res.json({
                    success: false,
                    msg: "Class or subject or chapter or user not found",
                });
            }
            else {
                const activity = M_Activity_1.Activity.create({
                    activity_name,
                    atoms,
                    url,
                    classes,
                    subject,
                    chapter,
                    user,
                });
                yield activity.save();
                res
                    .status(201)
                    .json({ success: true, msg: "Activity added Successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_getOne: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { activity_id } = req.params;
            const activity = yield M_Activity_1.Activity.findOneById(activity_id);
            if (!activity) {
                res.json({ success: false, msg: "activity not found" });
            }
            else {
                res.json({ success: true, activity });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_getGoal: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { goal_id } = req.params;
            const { user_id } = req.body;
            const act = yield GoalActivity_1.GoalActivity.find({ where: { goal_id } });
            const activity = yield M_Activity_1.Activity.findBy({
                id: (0, typeorm_1.In)(act.map((id) => id.activity_id)),
            });
            if (activity.length > 0) {
                const user_activity = yield U_Activity_1.U_Activity.find({ where: { user_id } });
                activity.map((i) => {
                    user_activity.map((j) => {
                        i["selected"] = false;
                        if (i.id == j.activity_id) {
                            i["selected"] = true;
                        }
                    });
                });
                res.json({ success: true, activity });
            }
            else {
                res.json({ success: false, msg: "activity not found" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    goal_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { class_id, activity_id, goal_name, week_id } = req.body;
            const classes = yield M_Class_1.Class.findOneById(class_id);
            const ids = activity_id.split(",");
            if (!classes) {
                res.json({ success: false, msg: "class not found" });
            }
            else {
                const goal = M_Goal_1.M_Goal.create({
                    goal_name,
                    week_id,
                    classes,
                });
                yield goal.save();
                for (let i = 0; i < ids.length; i++) {
                    const id = ids[i].trim();
                    const activity = yield M_Activity_1.Activity.findOneById(id);
                    if (!activity) {
                        res.json({ success: false, msg: "activity not found" });
                    }
                    const goal_activity = GoalActivity_1.GoalActivity.create({
                        goal_id: goal.id,
                        activity_id: id,
                    });
                    yield goal_activity.save();
                }
                res.status(201).json({ success: true, msg: "Goal added Successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    goal_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const goal = yield M_Goal_1.M_Goal.find();
            if (!goal) {
                res.json({ success: false, msg: "no goal found" });
            }
            else {
                res.json({ success: true, goal });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    week_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const week = (0, UserService_1.getWeek)();
            console.log(week);
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
};
//# sourceMappingURL=MasterController.js.map