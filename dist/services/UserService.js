"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.uploadVideo = exports.getStudentById = exports.resetPassword = exports.getMembers = exports.getWeek = exports.getActUser = exports.getAct = exports.deleteSocial = exports.deleteMember = exports.getStudent = exports.getActivityAtom = exports.getTeamAtom = exports.getMember = exports.getSubtopic = exports.getTopic = exports.getChapters = exports.getChapter = exports.getSubjects = exports.updateVerify = exports.updateOtp = exports.deleteLike = exports.getSocialLike = exports.getActivity = exports.getGoal = exports.getUser = void 0;
const typeorm_1 = require("typeorm");
const date_fns_1 = require("date-fns");
const M_Goal_1 = require("../model/Master/M_Goal");
const M_User_1 = require("../model/Auth/M_User");
const M_Activity_1 = require("../model/Master/M_Activity");
const M_Team_1 = require("../model/Master/M_Team");
const A_Social_Like_1 = require("../model/Activity/A_Social_Like");
const M_Subject_1 = require("../model/Master/M_Subject");
const M_Chapter_1 = require("../model/Master/M_Chapter");
const M_Topic_1 = require("../model/Master/M_Topic");
const M_SubTopic_1 = require("../model/Master/M_SubTopic");
const T_Team_Members_1 = require("../model/Activity/T_Team_Members");
const M_Student_1 = require("../model/Auth/M_Student");
const A_Social_1 = require("../model/Activity/A_Social");
const U_Activity_1 = require("../model/User/U_Activity");
function getUser(id) {
    return M_User_1.User.findOneById(id);
}
exports.getUser = getUser;
function getGoal(id) {
    return M_Goal_1.M_Goal.findOneById(id);
}
exports.getGoal = getGoal;
function getActivity(id) {
    return M_Activity_1.Activity.findOneById(id);
}
exports.getActivity = getActivity;
function getSocialLike(uId, sId) {
    return (0, typeorm_1.createQueryBuilder)(A_Social_Like_1.A_Social_Like, "a_social_like")
        .where("user_id=:uId", { uId })
        .andWhere("social_activity_id=:sId", {
        sId,
    })
        .execute();
}
exports.getSocialLike = getSocialLike;
function deleteLike(uId, sId) {
    (0, typeorm_1.createQueryBuilder)(A_Social_Like_1.A_Social_Like, "a_social_like")
        .delete()
        .where("user_id=:uId", { uId })
        .andWhere("social_activity_id=:sId", {
        sId,
    })
        .execute();
}
exports.deleteLike = deleteLike;
function updateOtp(otp, id) {
    (0, typeorm_1.createQueryBuilder)(M_User_1.User, "m_user")
        .update(M_User_1.User)
        .set({ otp })
        .where("id = :id", { id })
        .execute();
}
exports.updateOtp = updateOtp;
function resetPassword(id, password) {
    (0, typeorm_1.createQueryBuilder)(M_User_1.User, "m_user")
        .update(M_User_1.User)
        .set({ password })
        .where("id = :id", { id })
        .execute();
}
exports.resetPassword = resetPassword;
function updateVerify(id, password) {
    (0, typeorm_1.createQueryBuilder)(M_User_1.User, "m_user")
        .update(M_User_1.User)
        .set({ email_verified: true, password })
        .where("id = :id", { id })
        .execute();
}
exports.updateVerify = updateVerify;
function getSubjects(id) {
    return (0, typeorm_1.createQueryBuilder)(M_Subject_1.Subject, "m_subject")
        .where("m_subject.class_id = :id", { id })
        .getMany();
}
exports.getSubjects = getSubjects;
function getChapter() {
    return M_Chapter_1.Chapter.find({
        order: {
            category_name: "DESC",
        },
    });
}
exports.getChapter = getChapter;
function getChapters(id) {
    return (0, typeorm_1.createQueryBuilder)(M_Chapter_1.Chapter, "m_chapter")
        .where("m_chapter.subject_id = :id", { id })
        .getMany();
}
exports.getChapters = getChapters;
function getTopic(id) {
    return (0, typeorm_1.createQueryBuilder)(M_Topic_1.Topic, "m_topic")
        .where("m_topic.chapter_id = :id", { id })
        .getMany();
}
exports.getTopic = getTopic;
function getSubtopic(id) {
    return (0, typeorm_1.createQueryBuilder)(M_SubTopic_1.SubTopic, "m_subtopic")
        .where("m_subtopic.topic_id=:id", { id })
        .getMany();
}
exports.getSubtopic = getSubtopic;
function getMember(id) {
    return (0, typeorm_1.createQueryBuilder)(T_Team_Members_1.TeamMember, "team_members")
        .where("team_members.team_id=:id", { id })
        .getMany();
}
exports.getMember = getMember;
function getTeamAtom() {
    return M_Team_1.Team.find({ order: { team_atoms: "DESC" } });
}
exports.getTeamAtom = getTeamAtom;
function getActivityAtom() {
    return M_Activity_1.Activity.find({ order: { atoms: "DESC" } });
}
exports.getActivityAtom = getActivityAtom;
function getStudent() {
    return M_Student_1.Student.find({ order: { atoms: "DESC" } });
}
exports.getStudent = getStudent;
function getStudentById(id) {
    return M_Student_1.Student.findOneById(id);
}
exports.getStudentById = getStudentById;
function deleteMember(id, tId) {
    (0, typeorm_1.createQueryBuilder)(T_Team_Members_1.TeamMember, "team_members")
        .delete()
        .where("team_members.team_id=:tId", { tId })
        .andWhere("team_members.user_id=:id", { id })
        .execute();
}
exports.deleteMember = deleteMember;
function deleteSocial(aId, sId) {
    (0, typeorm_1.createQueryBuilder)(A_Social_1.A_Social, "a_social")
        .delete()
        .where("a_social.activity_id=:aId", { aId })
        .andWhere("a_social.social_activity_id=:sId", { sId })
        .execute();
}
exports.deleteSocial = deleteSocial;
function getAct(id) {
    return (0, typeorm_1.createQueryBuilder)(M_Activity_1.Activity, "m_activity")
        .where("m_activity.chapter_id=:id", { id })
        .getMany();
}
exports.getAct = getAct;
function getActUser(id) {
    return (0, typeorm_1.createQueryBuilder)(M_Activity_1.Activity, "m_activity")
        .where("m_activity.created_by=:id", { id })
        .getMany();
}
exports.getActUser = getActUser;
function getWeek() {
    const week = (0, date_fns_1.eachWeekOfInterval)({
        start: new Date(2022, 8, 1),
        end: new Date(2023, 10, 5),
    }, { weekStartsOn: 2 });
    return week;
}
exports.getWeek = getWeek;
const getMembers = (id) => __awaiter(void 0, void 0, void 0, function* () {
    return yield (0, typeorm_1.createQueryBuilder)(T_Team_Members_1.TeamMember, "team_members")
        .where("team_members.user_id=:id", { id })
        .getMany();
});
exports.getMembers = getMembers;
function uploadVideo(id, aId, url) {
    const video_url = `https://piehub.s3.ap-south-1.amazonaws.com/${url}`;
    (0, typeorm_1.createQueryBuilder)(U_Activity_1.U_Activity, "u_activity")
        .update(U_Activity_1.U_Activity)
        .set({ video_url })
        .where("user_id = :id", { id })
        .andWhere("activity_id=:aId", { aId })
        .execute();
}
exports.uploadVideo = uploadVideo;
//# sourceMappingURL=UserService.js.map