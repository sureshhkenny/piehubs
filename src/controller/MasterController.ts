import { Request, Response } from "express";
import { Class } from "../model/Master/M_Class";
import { Subject } from "../model/Master/M_Subject";
import { Chapter } from "../model/Master/M_Chapter";
import { Topic } from "../model/Master/M_Topic";
import { SubTopic } from "../model/Master/M_SubTopic";
import { Activity } from "../model/Master/M_Activity";
import { M_Goal } from "../model/Master/M_Goal";
import { GoalActivity } from "../model/Transaction/GoalActivity";
import {
  getUser,
  getSubjects,
  getChapter,
  getChapters,
  getTopic,
  getSubtopic,
  getAct,
  getActUser,
  getWeek,
} from "../services/UserService";
import { In } from "typeorm";
import { U_Activity } from "../model/User/U_Activity";

export default {
  class_get: async (req: Request, res: Response) => {
    try {
      const stnds = await Class.find();
      res.json({ success: true, class: stnds });
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  class_post: async (req: Request, res: Response) => {
    try {
      const stnds = Class.create({
        class_name: req.body.class_name,
      });
      await stnds.save();
      res.status(201).json({ success: true, msg: "Class added successfully" });
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  subject_get: async (req: Request, res: Response) => {
    try {
      const { class_id } = req.params;
      const classId = await Class.findOneById(class_id);
      const subject = await getSubjects(class_id);
      if (!classId || !subject) {
        res.json({
          success: false,
          msg: "Class not found or no subject for this class",
        });
      } else {
        res.json({ success: true, subject });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  subject_post: async (req: Request, res: Response) => {
    try {
      const { class_id } = req.params;
      const classes = await Class.findOneById(class_id);
      if (!classes) {
        res.json({ success: false, msg: "Class not found" });
      } else {
        const subject = Subject.create({
          subject_name: req.body.subject_name,
          classes,
        });
        await subject.save();
        res
          .status(201)
          .json({ success: true, msg: "Subject added Successfully" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  subject_read: async (req: Request, res: Response) => {
    try {
      const subject = await Subject.find();
      if (!subject) {
        res.json({ success: false, msg: "Subject not found" });
      } else {
        res.json({ success: true, subject });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  chapter_read: async (req: Request, res: Response) => {
    try {
      const chapter = await Chapter.find();
      if (!chapter) {
        res.json({ success: false, msg: "Chapter not found" });
      } else {
        res.json({ success: true, chapter });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  chapter_category: async (req: Request, res: Response) => {
    try {
      const chapter = getChapter();
      if (!chapter) {
        res.json({ success: false, msg: "no chapter found" });
      } else {
        res.send({ success: true, chapter });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  chapter_get: async (req: Request, res: Response) => {
    try {
      const { subject_id } = req.params;
      const subId = await Subject.findOneById(subject_id);
      const chapter = await getChapters(subject_id);
      if (!subId || !chapter) {
        res.json({ success: false, msg: "Subject or chapter not found" });
      } else {
        res.json({ success: true, chapter });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  chapter_post: async (req: Request, res: Response) => {
    try {
      const { subject_id } = req.params;
      const { chapter_name, category_name } = req.body;
      const subject = await Subject.findOneById(subject_id);
      if (!subject) {
        res.json({ success: false, msg: "Subject not found" });
      } else {
        const chapter = Chapter.create({
          chapter_name,
          category_name,
          subject,
        });
        await chapter.save();
        res
          .status(201)
          .json({ success: true, msg: "Chapter added Successfully" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  chapter_one_category: async (req: Request, res: Response) => {
    try {
      const { name } = req.params;
      const chapter = await Chapter.find({ where: { category_name: name } });
      if (chapter.length < 1) {
        res.json({ success: false, msg: "chapter not found" });
      } else {
        res.json({ success: true, chapter });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  topic_get: async (req: Request, res: Response) => {
    try {
      const { chapter_id } = req.params;
      const chapterId = await Chapter.findOneById(chapter_id);
      const topic = await getTopic(chapter_id);
      if (!chapterId || !topic) {
        res.json({ success: false, msg: "Chapter or topic not found" });
      } else {
        res.json({ success: true, topic });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  topic_post: async (req: Request, res: Response) => {
    try {
      const { chapter_id } = req.params;
      const chapter = await Chapter.findOneById(chapter_id);
      if (!chapter) {
        res.json({ success: false, msg: "Chapter not found" });
      } else {
        const topic = Topic.create({
          topic_name: req.body.topic_name,
          chapter,
        });
        await topic.save();
        res
          .status(201)
          .json({ success: true, msg: "Topic added Successfully" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  topic_read: async (req: Request, res: Response) => {
    try {
      const topic = await Topic.find();
      if (!topic) {
        res.json({ success: false, msg: "Topic not found" });
      } else {
        res.json({ success: true, topic });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  subtopic_get: async (req: Request, res: Response) => {
    try {
      const { topic_id } = req.params;
      const topicId = await Topic.findOneById(topic_id);
      const subtopic = await getSubtopic(topic_id);
      if (!topicId || !subtopic) {
        res.json({ success: false, msg: "Topic not found" });
      } else {
        res.json({ success: true, subtopic });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  subtopic_post: async (req: Request, res: Response) => {
    try {
      const { topic_id } = req.params;
      const { subtopic_name, url } = req.body;
      const topic = await Topic.findOneById(topic_id);
      if (!topic) {
        res.json({ success: false, msg: "Topic not found" });
      } else {
        const subtopic = SubTopic.create({
          subtopic_name,
          url,
          topic,
        });
        await subtopic.save();
        res
          .status(201)
          .json({ success: true, msg: "Subtopic added Successfully" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  subtopic_read: async (req: Request, res: Response) => {
    try {
      const subtopic = await SubTopic.find();
      if (!subtopic) {
        res.json({ success: false, msg: "Subtopics not found" });
      } else {
        res.json({ success: true, subtopic });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  activity_read: async (req: Request, res: Response) => {
    try {
      const activity = await Activity.find();
      if (!activity) {
        res.json({ success: false, msg: "Activity not found" });
      } else {
        res.json({ success: true, activity });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  activity_get: async (req: Request, res: Response) => {
    try {
      const { chapter_id } = req.params;
      const chapter = await Chapter.findOneById(chapter_id);
      if (!chapter) {
        res.json({ success: false, msg: "Activity not found" });
      } else {
        const activity = await getAct(chapter_id);
        res.json({ success: true, activity });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  activity_getUser: async (req: Request, res: Response) => {
    try {
      const { class_id, subject_id, chapter_id } = req.body;
      const { user_id } = req.params;
      const stnd = await Class.findOneById(class_id);
      const subject = await Subject.findOneById(subject_id);
      const chapter = await Chapter.findOneById(chapter_id);
      const user = await getUser(user_id);
      if (!stnd || !subject || !chapter || !user) {
        res.json({ success: false, msg: "Activity not found" });
      } else {
        const activity = await getActUser(user_id);
        res.json({ success: true, activity });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  activity_post: async (req: Request, res: Response) => {
    try {
      const { class_id, subject_id, chapter_id, activity_name, atoms, url } =
        req.body;
      const { user_id } = req.params;
      const classes = await Class.findOneById(class_id);
      const subject = await Subject.findOneById(subject_id);
      const chapter = await Chapter.findOneById(chapter_id);
      const user = await getUser(user_id);
      if (!classes || !subject || !chapter || !user) {
        res.json({
          success: false,
          msg: "Class or subject or chapter or user not found",
        });
      } else {
        const activity = Activity.create({
          activity_name,
          atoms,
          url,
          classes,
          subject,
          chapter,
          user,
        });
        await activity.save();
        res
          .status(201)
          .json({ success: true, msg: "Activity added Successfully" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  activity_getOne: async (req: Request, res: Response) => {
    try {
      const { activity_id } = req.params;
      const activity = await Activity.findOneById(activity_id);
      if (!activity) {
        res.json({ success: false, msg: "activity not found" });
      } else {
        res.json({ success: true, activity });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  activity_getGoal: async (req: Request, res: Response) => {
    try {
      const { goal_id } = req.params;
      const { user_id } = req.body;
      const act = await GoalActivity.find({ where: { goal_id } });
      const activity = await Activity.findBy({
        id: In(act.map((id) => id.activity_id)),
      });
      if (activity.length > 0) {
        const user_activity = await U_Activity.find({ where: { user_id } });
        activity.map((i) => {
          user_activity.map((j) => {
            i["selected"] = false;
            if (i.id == j.activity_id) {
              i["selected"] = true;
            }
          });
        });
        res.json({ success: true, activity });
      } else {
        res.json({ success: false, msg: "activity not found" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  goal_post: async (req: Request, res: Response) => {
    try {
      const { class_id, activity_id, goal_name, week_id } = req.body;
      const classes = await Class.findOneById(class_id);
      const ids = activity_id.split(",");
      if (!classes) {
        res.json({ success: false, msg: "class not found" });
      } else {
        const goal = M_Goal.create({
          goal_name,
          week_id,
          classes,
        });
        await goal.save();
        for (let i = 0; i < ids.length; i++) {
          const id = ids[i].trim();
          const activity = await Activity.findOneById(id);
          if (!activity) {
            res.json({ success: false, msg: "activity not found" });
          }
          const goal_activity = GoalActivity.create({
            goal_id: goal.id,
            activity_id: id,
          });
          await goal_activity.save();
        }
        res.status(201).json({ success: true, msg: "Goal added Successfully" });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
  goal_get: async (req: Request, res: Response) => {
    try {
      const goal = await M_Goal.find();
      if (!goal) {
        res.json({ success: false, msg: "no goal found" });
      } else {
        res.json({ success: true, goal });
      }
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },

  week_get: async (req: Request, res: Response) => {
    try {
      const week = getWeek();
      console.log(week);
    } catch (err) {
      console.log(err);
      res.json({ success: false, msg: "invalid request" });
    }
  },
};
