import { Request, Response, NextFunction } from "express";

const validation =
  (schema) => async (req: Request, res: Response, next: NextFunction) => {
    const body = req.body;
    try {
      await schema.validate(body);
      next();
    } catch (err: any) {
      res.json({ success: false, msg: err.message });
    }
  };

export default validation;
