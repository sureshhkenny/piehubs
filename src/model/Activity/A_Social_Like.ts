import { Entity, Column } from "typeorm";
import { Base } from "../Base";

@Entity("a_social_like")
export class A_Social_Like extends Base {
  @Column()
  user_id?: string;
  @Column()
  social_activity_id?: string;
  @Column({ default: false })
  status?: Boolean;
}
