import { Router } from "express";
import multer from "multer";
import Act from "../controller/ActivityController";
import checkToken from "../middleware/auth_student";
import Master from "../controller/MasterController";

const Activity = Router();
Activity.use(checkToken);
const storage = multer.memoryStorage();
const upload = multer({ storage });

//  **************  Team    **************
Activity.post("/team", Act.team_post);
Activity.get("/team", Act.team_get);
Activity.post("/team/:team_id/members", Act.team_member_post);
Activity.get("/team/:team_id/members", Act.team_member_get);
Activity.delete("/team/:team_id/members", Act.team_member_delete);
Activity.get("/team_leader_board", Act.team_leaderboard);
Activity.get("/team/user/:user_id", Act.my_team_get);
Activity.put("/team/:team_id", Act.updateTeam);

//  ************** Social Activity  **************
Activity.post("/social/activity/:activity_id", Act.social_post);
Activity.get("/social/activity/:activity_id", Act.social_get);
Activity.delete("/social/activity/:activity_id", Act.social_delete);
Activity.post("/social/:social_activity_id/like", Act.social_like_post);
Activity.get("/social/:social_activity_id/like", Act.social_like_get);
Activity.post("/social/:social_activity_id/view", Act.social_view_post);
Activity.get("/social/:social_activity_id/view", Act.social_view_get);

//  **************  Activity    **************
Activity.post(
  "/activity/:activity_id/user/:user_id/upload",
  upload.single("video"),
  Act.u_activity_upload
);
Activity.get("/activity/user/:user_id", Act.u_activity_get);
Activity.post("/activity/:activity_id/user", Act.u_activity_add);

//  **************  Goal        **************
Activity.post("/goal/user/:user_id", Act.u_goal_post);
Activity.get("/goal/user/:user_id", Act.u_goal_get);
Activity.get("/activity", Master.activity_read);
Activity.get("/activity/:activity_id", Master.activity_getOne);
Activity.get("/activity/goal/:goal_id", Master.activity_getGoal);
Activity.get("/goal", Master.goal_get);

//  ************    Master      ***********
Activity.get("/scientists_leader_board", Act.student_leaderboard);
Activity.get("/chapter", Master.chapter_read);
Activity.get("/category/:name", Master.chapter_one_category);
Activity.get("/topic", Master.topic_read);
Activity.get("/topic/chapter/:chapter_id", Master.topic_get);
Activity.get("/subtopic", Master.subtopic_read);
Activity.get("/subtopic/topic/:topic_id", Master.subtopic_get);
Activity.get("/scientist/:scientist_id", Act.user_info_get);
Activity.put("/scientist/:scientist_id", Act.update_profile);

export default Activity;
