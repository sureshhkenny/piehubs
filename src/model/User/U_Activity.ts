import { Entity, Column, OneToMany } from "typeorm";
import { A_Social } from "../Activity/A_Social";
import { Base } from "../Base";

@Entity("u_activity")
export class U_Activity extends Base {
  @Column({ nullable: true })
  comments?: string;
  @Column({ nullable: true })
  atoms_scored?: number;
  @Column({ default: false })
  status?: boolean;
  @Column({ nullable: true })
  video_url?: string;
  @Column()
  user_id?: string;
  @Column()
  activity_id?: string;

  @OneToMany(() => A_Social, (social: any) => social.activity)
  social?: A_Social[];
}
