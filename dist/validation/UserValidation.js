"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userSchema = exports.signupSchema = void 0;
const yup_1 = require("yup");
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const signupSchema = (0, yup_1.object)().shape({
    email: (0, yup_1.string)().email().required(),
    mobile: (0, yup_1.string)().matches(phoneRegExp, "Phone number is not valid").required(),
});
exports.signupSchema = signupSchema;
const userSchema = (0, yup_1.object)().shape({
    first_name: (0, yup_1.string)().min(3).required(),
    last_name: (0, yup_1.string)().required(),
    password: (0, yup_1.string)().min(6).max(12).required(),
    school_name: (0, yup_1.string)().min(3).required(),
    standard: (0, yup_1.number)().max(15).required(),
});
exports.userSchema = userSchema;
//# sourceMappingURL=UserValidation.js.map