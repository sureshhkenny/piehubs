import { number, object, string } from "yup";

const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const signupSchema = object().shape({
  email: string().email().required(),
  mobile: string().matches(phoneRegExp, "Phone number is not valid").required(),
});

const userSchema = object().shape({
  first_name: string().min(3).required(),
  last_name: string().required(),
  password: string().min(6).max(12).required(),
  school_name: string().min(3).required(),
  standard: number().max(15).required(),
});

export { signupSchema, userSchema };
