import { Entity, Column } from "typeorm";
import { Base } from "../Base";
@Entity("m_student")
export class Student extends Base {
  @Column()
  first_name?: string;
  @Column()
  last_name?: string;
  @Column({ type: "bigint" })
  mobile?: number;
  @Column()
  school_name?: string;
  @Column()
  standard?: number;
  @Column({ nullable: true })
  atoms?: number;
  @Column({ nullable: true })
  profile_pic?: string;
  @Column({ nullable: true })
  father_name?: string;
  @Column({ type: "bigint", nullable: true })
  father_mobile?: number;
  @Column({ nullable: true })
  father_email?: string;
  @Column({ nullable: true })
  mother_name?: string;
  @Column({ type: "bigint", nullable: true })
  mother_mobile?: number;
  @Column({ nullable: true })
  mother_email?: string;
  @Column({ unique: true })
  user_id?: string;
}
