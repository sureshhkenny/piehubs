"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const U_Goal_1 = require("../model/User/U_Goal");
const U_Activity_1 = require("../model/User/U_Activity");
const T_Team_Members_1 = require("../model/Activity/T_Team_Members");
const M_Team_1 = require("../model/Master/M_Team");
const A_Social_1 = require("../model/Activity/A_Social");
const A_Social_Like_1 = require("../model/Activity/A_Social_Like");
const A_Social_View_1 = require("../model/Activity/A_Social_View");
const enum_1 = require("../interface/enum");
const UserService_1 = require("../services/UserService");
const typeorm_1 = require("typeorm");
const s3_bucket_1 = require("../middleware/s3_bucket");
const M_Student_1 = require("../model/Auth/M_Student");
exports.default = {
    team_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { user_id, team_name, team_pic } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!user) {
                res.json({ suceess: false, msg: "user not found" });
            }
            else {
                const team = M_Team_1.Team.create({ user, team_name, team_pic });
                yield team.save();
                const member = T_Team_Members_1.TeamMember.create({ user_id, team_id: team.id });
                yield member.save();
                res.status(201).json({ success: true, msg: "team added successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    team_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const teams = yield M_Team_1.Team.find();
            if (!teams) {
                res.json({ suceess: false, msg: "team not found" });
            }
            else {
                res.json({ success: true, teams });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    u_goal_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { user_id } = req.params;
            const { goal_id, activity_id, atoms_scored } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            const goal = yield (0, UserService_1.getGoal)(goal_id);
            const activity = yield (0, UserService_1.getActivity)(activity_id);
            if (!user || !goal || !activity) {
                res.json({ success: false, msg: "user or goal or activity not found" });
            }
            else {
                const u_goal = U_Goal_1.U_Goal.create({
                    user,
                    activity,
                    goal,
                    atoms_scored,
                });
                yield u_goal.save();
                res
                    .status(201)
                    .json({ success: true, msg: "User goal added successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    u_goal_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { user_id } = req.params;
            const user = yield (0, UserService_1.getUser)(user_id);
            const user_goal = yield U_Goal_1.U_Goal.find();
            if (!user || !user_goal) {
                res.json({ success: false, msg: "user or user goal not found" });
            }
            else {
                res.json({ success: true, user_goal });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    u_activity_upload: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { activity_id, user_id } = req.params;
            const file = req.file;
            const activity = yield (0, UserService_1.getActivity)(activity_id);
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!user || !activity || !file) {
                res.json({ success: false, msg: "user or activity or file not found" });
            }
            else {
                const name = Date.now().toString() + "_" + file.originalname;
                (0, UserService_1.uploadVideo)(user_id, activity_id, name);
                yield (0, s3_bucket_1.uploadFile)(file, name);
                res.json({ success: true, msg: "video added successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    u_activity_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { user_id } = req.params;
            const user = yield (0, UserService_1.getUser)(user_id);
            const user_activity = yield U_Activity_1.U_Activity.find();
            if (!user || !user_activity) {
                res.json({ suceess: false, msg: "user or user activity not found" });
            }
            else {
                res.json({ success: true, user_activity });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    team_member_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { team_id } = req.params;
            const team = yield M_Team_1.Team.findOneById(team_id);
            const { user_id } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!team || !user) {
                res.json({ suceess: false, msg: "user or team not found" });
            }
            else {
                const member = yield (0, UserService_1.getMember)(team_id);
                if (member.length > 4) {
                    res.json({ success: false, msg: "team member can't more than 3" });
                }
                else {
                    const id = yield T_Team_Members_1.TeamMember.find({ where: { team_id, user_id } });
                    if (!id) {
                        const t_member = T_Team_Members_1.TeamMember.create({ team_id, user_id });
                        yield t_member.save();
                        res
                            .status(201)
                            .json({ success: true, msg: "team member added successfully" });
                    }
                    else {
                        res.json({ success: false, msg: "User already exists" });
                    }
                }
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    team_member_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { team_id } = req.params;
            const team = yield M_Team_1.Team.findOneById(team_id);
            const team_members = yield T_Team_Members_1.TeamMember.find();
            if (!team || !team_members) {
                res.json({ suceess: false, msg: "team or team members not found" });
            }
            else {
                res.json({ success: true, team_members });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    team_member_delete: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { team_id } = req.params;
            const team = yield M_Team_1.Team.findOneById(team_id);
            const { user_id } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!team || !user) {
                res.json({ success: false, msg: "user or team not found" });
            }
            else {
                (0, UserService_1.deleteMember)(user_id, team_id);
                res
                    .status(202)
                    .json({ success: true, msg: "member deleted successsfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { activity_id } = req.params;
            const { user_id, video_url, atoms_scored } = req.body;
            const activity = yield (0, UserService_1.getActivity)(activity_id);
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!activity || !user) {
                res.json({ suceess: false, msg: "user or activity not found" });
            }
            else {
                const social = A_Social_1.A_Social.create({
                    activity,
                    user,
                    video_url,
                    atoms_scored,
                });
                yield social.save();
                res
                    .status(201)
                    .json({ success: true, msg: "social activity added successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { activity_id } = req.params;
            const { user_id } = req.body;
            const activity = yield (0, UserService_1.getActivity)(activity_id);
            const posts = yield A_Social_1.A_Social.find();
            if (!activity || !posts) {
                res.json({
                    suceess: false,
                    msg: "activity or social activity not found",
                });
            }
            else {
                const likes = yield A_Social_Like_1.A_Social_Like.find({
                    where: {
                        user_id,
                    },
                });
                posts.map((post) => {
                    likes.map((like) => {
                        post["liked"] = false;
                        if (post.id == like.social_activity_id) {
                            post["liked"] = true;
                        }
                    });
                });
                res.json({ success: true, posts });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_delete: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { activity_id } = req.params;
            const { social_activity_id } = req.body;
            const activity = yield (0, UserService_1.getActivity)(activity_id);
            const social_activity = yield A_Social_1.A_Social.findOneById(social_activity_id);
            if (!activity || social_activity) {
                res.json({
                    success: false,
                    msg: "activity or social activity not found",
                });
            }
            else {
                (0, UserService_1.deleteSocial)(activity_id, social_activity_id);
                res
                    .status(202)
                    .json({ success: true, msg: "social activity successfully deleted" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_like_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { social_activity_id } = req.params;
            const { user_id, type } = req.body;
            const social = yield A_Social_1.A_Social.findOneById(social_activity_id);
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!user || !social) {
                res.json({ suceess: false, msg: "user or social activity not found" });
            }
            else if (social.like) {
                if (type === enum_1.TransactionType.LIKE) {
                    const social_like_data = yield (0, UserService_1.getSocialLike)(user_id, social_activity_id);
                    if (social_like_data.length > 0) {
                        res.json({ success: false, msg: "already liked" });
                    }
                    else {
                        const social_like = A_Social_Like_1.A_Social_Like.create({
                            user_id,
                            social_activity_id,
                            status: true,
                        });
                        yield social_like.save();
                        social.like = 1 + parseInt(social.like.toString());
                        res.status(201).json({
                            success: true,
                            msg: "social activity liked successfully",
                        });
                    }
                }
                else if (type === enum_1.TransactionType.UNLIKE) {
                    (0, UserService_1.deleteLike)(user_id, social_activity_id);
                    social.like = 1 - parseInt(social.like.toString());
                    res.status(201).json({
                        success: true,
                        msg: "social activity unliked successfully",
                    });
                }
                yield social.save();
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_like_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { social_activity_id } = req.params;
            const social = yield A_Social_1.A_Social.findOneById(social_activity_id);
            const social_like = yield A_Social_Like_1.A_Social_Like.find();
            if (!social || !social_like) {
                res.json({
                    suceess: false,
                    msg: "social activity not found or no likes for this activity",
                });
            }
            else {
                res.json({ success: true, social_like });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_view_post: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { social_activity_id } = req.params;
            const { user_id } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            const social = yield A_Social_1.A_Social.findOneById(social_activity_id);
            if (!social || !user) {
                res.json({ suceess: false, msg: "user or social activity not found" });
            }
            else {
                const social_view = A_Social_View_1.A_Social_View.create({ user, social });
                yield social_view.save();
                if (social.view) {
                    social.view = 1 + parseInt(social.view.toString());
                    yield social.save();
                    res.status(201).json({ success: true, msg: "activity viewed" });
                }
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    social_view_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { social_activity_id } = req.params;
            const social = yield A_Social_1.A_Social.findOneById(social_activity_id);
            const social_view = yield A_Social_View_1.A_Social_View.find();
            if (!social || !social_view) {
                res.json({
                    suceess: false,
                    msg: "social activity not found or no views for this post",
                });
            }
            else {
                res.json({ success: true, social_view });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    team_leaderboard: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const team = yield (0, UserService_1.getTeamAtom)();
            res.json({ success: true, team });
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    student_leaderboard: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const student = yield (0, UserService_1.getStudent)();
            res.json({ success: true, student });
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    activity_leaderboard: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const activity = (0, UserService_1.getActivityAtom)();
            res.json({ success: true, activity });
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    my_team_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { user_id } = req.params;
            const team = yield (0, UserService_1.getMembers)(user_id);
            const teams = yield M_Team_1.Team.findBy({
                id: (0, typeorm_1.In)(team.map((id) => id.team_id)),
            });
            if (teams.length > 0) {
                res.json({ success: true, teams });
            }
            else {
                res.json({ success: false, msg: "no teams found for the user" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    u_activity_add: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { activity_id } = req.params;
            const { user_id } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            const activity = yield (0, UserService_1.getActivity)(activity_id);
            if (!user || !activity) {
                res.json({ success: false, msg: "user or activity not found" });
            }
            else {
                const user_activity = U_Activity_1.U_Activity.create({ user_id, activity_id });
                yield user_activity.save();
                res.json({ success: true, msg: "user activity added successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    user_info_get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { scientist_id } = req.params;
            const student = yield (0, UserService_1.getStudentById)(scientist_id);
            if (!student) {
                res.json({ success: false, msg: "Scientist not found" });
            }
            else {
                res.json({ success: true, student });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    update_profile: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { scientist_id } = req.params;
            const student = yield (0, UserService_1.getStudentById)(scientist_id);
            if (!student) {
                res.json({ success: false, msg: "Scientist not found" });
            }
            else {
                const update = yield M_Student_1.Student.merge(student, req.body);
                yield update.save();
                res.json({ success: true, msg: "profile updated successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    updateTeam: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { team_id } = req.params;
            const team = yield M_Team_1.Team.findOneById(team_id);
            if (!team) {
                res.json({ success: false, msg: "team not found" });
            }
            else {
                const update = yield M_Team_1.Team.merge(team, req.body);
                yield update.save();
                res.json({ success: true, msg: "team updated successfully" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
};
//# sourceMappingURL=ActivityController.js.map