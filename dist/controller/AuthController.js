"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mailer_1 = __importDefault(require("../config/mailer"));
const M_User_1 = require("../model/Auth/M_User");
const M_Student_1 = require("../model/Auth/M_Student");
const UserService_1 = require("../services/UserService");
exports.default = {
    signup: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { email, mobile } = req.body;
            const users = yield M_User_1.User.findOneBy({ email });
            if (users === null || users === void 0 ? void 0 : users.email_verified) {
                res.json({ success: false, msg: "Email already exists" });
            }
            else {
                let transporter = nodemailer_1.default.createTransport(Object.assign({}, mailer_1.default));
                let otp = Math.floor(100000 + Math.random() * 900000);
                transporter
                    .sendMail({
                    from: "genie@truetechsolutions.in",
                    to: email,
                    subject: "Email OTP Verification",
                    html: `<!DOCTYPE html><html><head><title></title></head><body><div><p>Hello,</p><p>Welcome to Pie Hub Labs</p><p>Please use the following OTP ${otp} to complete your sign-up process.</p><p>Enter the code and get connected.</p><p>Best Wishes,</p><p>Pie Hub Labs</p></div></body></html>`,
                })
                    .catch(console.error);
                if (!users) {
                    const user = M_User_1.User.create({
                        email,
                        mobile,
                        otp,
                    });
                    yield user.save();
                }
                else if (users.id) {
                    (0, UserService_1.updateOtp)(otp, users.id);
                }
                res
                    .status(201)
                    .json({ success: true, msg: "Please verify email", otp });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    signin: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { email, password } = req.body;
            const user = yield M_User_1.User.findOneBy({ email });
            if (!user) {
                res.json({ success: false, msg: "Invalid credentials" });
            }
            else {
                if ((yield bcrypt_1.default.compare(password, `${user.password}`)) &&
                    (user.email_verified || user.user_role == "admin")) {
                    const token = jsonwebtoken_1.default.sign({ userId: user.id }, `${process.env.ACCESS_TOKEN_SECRET}`);
                    if (user.user_role == "student") {
                        const student = yield M_Student_1.Student.findOneBy({ user_id: user.id });
                        res.status(200).json({
                            success: true,
                            msg: "Successfully logged in",
                            token,
                            student,
                        });
                    }
                    else {
                        res.status(200).json({
                            success: true,
                            msg: "Successfully logged in",
                            token,
                            user_id: user.id,
                        });
                    }
                }
                else {
                    res.json({ success: false, msg: "Invalid credentials" });
                }
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    verification: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { email, otp } = req.body;
            if (email && otp) {
                const user = yield M_User_1.User.findOneBy({ email });
                if (!user || user.otp != otp) {
                    res.json({ success: false, msg: "Invalid credentials" });
                }
                else {
                    res.status(200).json({
                        success: true,
                        msg: "Otp Verified Successfully",
                        id: user.id,
                    });
                }
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    register: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { first_name, last_name, password, school_name, standard } = req.body;
            const { user_id } = req.params;
            const hashpassword = yield bcrypt_1.default.hash(password, 10);
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!user) {
                res.json({ success: false, msg: "Invalid User" });
            }
            else {
                const student = M_Student_1.Student.create({
                    first_name,
                    last_name,
                    school_name,
                    standard,
                    mobile: user.mobile,
                    user_id,
                });
                yield student.save();
                (0, UserService_1.updateVerify)(user_id, hashpassword);
                const token = jsonwebtoken_1.default.sign({ userId: user.id }, `${process.env.ACCESS_TOKEN_SECRET}`);
                res.status(200).json({
                    success: true,
                    msg: "Successfully registered",
                    token,
                    student,
                });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    get: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield M_User_1.User.find();
        res.json({ success: true, msg: "success", user });
    }),
    admin: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        const { email, password } = req.body;
        try {
            const hashpassword = yield bcrypt_1.default.hash(password, 10);
            const user = M_User_1.User.create({
                email,
                password: hashpassword,
                user_role: "admin",
                mobile: 9876543210,
            });
            yield user.save();
            const token = jsonwebtoken_1.default.sign({ userId: user.id }, `${process.env.ACCESS_TOKEN_SECRET}`);
            res.status(201).json({ success: true, token });
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    forgot_password: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { email } = req.body;
            const user = yield M_User_1.User.findOneBy({ email });
            if (user === null || user === void 0 ? void 0 : user.id) {
                let otp = Math.floor(100000 + Math.random() * 900000);
                (0, UserService_1.updateOtp)(otp, user.id);
                res.json({ success: true, otp, user });
            }
            else {
                res.json({ success: false, msg: "invalid credentials" });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
    reset_password: (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const { user_id, otp, password } = req.body;
            const user = yield (0, UserService_1.getUser)(user_id);
            if (!user || user.otp != otp) {
                res.json({ success: false, msg: "invalid credentials" });
            }
            else {
                const hashpassword = yield bcrypt_1.default.hash(password, 10);
                (0, UserService_1.resetPassword)(user_id, hashpassword);
                res.json({ success: true, msg: "Password reset Successfully", user });
            }
        }
        catch (err) {
            console.log(err);
            res.json({ success: false, msg: "invalid request" });
        }
    }),
};
//# sourceMappingURL=AuthController.js.map